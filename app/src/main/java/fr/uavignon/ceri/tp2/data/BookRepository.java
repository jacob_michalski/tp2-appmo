package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Update;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {
    private MutableLiveData<Book> selectedBook = new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;

    private BookDAO bookDAO;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDAO = db.bookDAO();
        allBooks = bookDAO.getAllBooks();
    }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }

    public void updateBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            bookDAO.updateBook(book);
        });
    }

    public void insertBook(Book newbook) {
        databaseWriteExecutor.execute(() -> {
            bookDAO.insertBook(newbook);
        });
    }

    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> {
            bookDAO.deleteBook(id);
        });
    }

    public void getBook(long id) {

        Future<Book> fbooks = databaseWriteExecutor.submit(() -> {
            return bookDAO.getBook(id);
        });
        try {
            selectedBook.setValue(fbooks.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
