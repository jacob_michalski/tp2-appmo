package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.DetailViewModel;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;
    DetailViewModel viewModel;
    Book selectedBook;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        //Book book = Book.books[(int)args.getBookNum()];
        if (args.getBookNum() < 0) {
            selectedBook = new Book("","","","","");
            selectedBook.setId(-1);
        }
        else {
            viewModel.getBook(args.getBookNum());
            selectedBook = viewModel.getSelectedBook().getValue();
        }
        textTitle = view.findViewById(R.id.nameBook);
        textAuthors = view.findViewById(R.id.editAuthors);
        textYear = view.findViewById(R.id.editYear);
        textGenres = view.findViewById(R.id.editGenres);
        textPublisher = view.findViewById(R.id.editPublisher);

        //textTitle.setText(book.getTitle());
        //textAuthors.setText(book.getAuthors());
        //textYear.setText(book.getYear());
        //textGenres.setText(book.getGenres());
        //textPublisher.setText(book.getPublisher());

        observerSetup();

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = textTitle.getText().toString();
                String authors = textAuthors.getText().toString();
                if (!title.equals("") && !authors.equals("")) {
                    selectedBook.setTitle(textTitle.getText().toString());
                    selectedBook.setAuthors(textAuthors.getText().toString());
                    selectedBook.setYear(textYear.getText().toString());
                    selectedBook.setGenres(textGenres.getText().toString());
                    selectedBook.setPublisher(textPublisher.getText().toString());
                    viewModel.insertOrUpdateBook(selectedBook);
                    Snackbar.make(view, "Modification enregistrée", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                } else {
                    if (title.equals("")) {
                        textTitle.setText("Saisir titre");
                    }
                    if (authors.equals("")) {
                        textAuthors.setText("Saisir auteur(s)");
                    }
                }

            }
        });

        view.findViewById(R.id.buttonDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.deleteBook(selectedBook.getId());
                Snackbar.make(view, "Livre supprimé", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);

            }
        });
    }

    private void observerSetup() {
        viewModel.getSelectedBook().observe(getViewLifecycleOwner(),
                new Observer<Book>() {
                    @Override
                    public void onChanged(@Nullable final Book book) {
                        textTitle.setText(book.getTitle());
                        textAuthors.setText(book.getAuthors());
                        textYear.setText(book.getYear());
                        textGenres.setText(book.getGenres());
                        textPublisher.setText(book.getPublisher());
                    }
                });
    }
}