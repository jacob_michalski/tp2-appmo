package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;
    private MutableLiveData<Book> selectedBook;

    public DetailViewModel(Application application) {
        super(application);
        repository = new BookRepository(application);
        selectedBook = repository.getSelectedBook();
    }

    public MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }
    public void insertOrUpdateBook(Book book) {
        if (book.getId() == -1) {
            Book newBook = new Book(book.getTitle(), book.getAuthors(), book.getYear(), book.getGenres(), book.getPublisher());
            repository.insertBook(newBook);
        }
        else {
            repository.updateBook(book);
        }
    }
    public void getBook(long id) {
        repository.getBook(id);
    }
    public void deleteBook(long id) {
        repository.deleteBook(id);
    }
}
